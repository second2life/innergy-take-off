import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { InputData } from './models/input-data.type';
import { MaterialData, OutputData } from './models/output-data.type';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  private inputData: InputData[];
  public outputData: OutputData[];
  public isDataFinished$: Subject<boolean> = new Subject();
  private subscription: Subscription;
  constructor() {}

  public ngOnInit(): void {
    this.subscription = this.isDataFinished$.subscribe((isFinished) => {
      if (isFinished) {
        this.mapToOutput();
        this.outputData = this.outputData.sort(this.sortWarehouses);
        this.outputData.forEach((outData) => {
          outData.materials.sort(this.sortMaterials);
        });
      }
    });
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public async handleFileInput(files: File): Promise<void> {
    this.reset();
    const fileReader = new FileReader();
    fileReader.onload = () => this.onFileLoad(fileReader.result as string);
    fileReader.readAsBinaryString(files[0]);
  }

  public reset(): void {
    this.inputData = [];
    this.outputData = [];
    this.isDataFinished$.next(false);
  }

  private sortWarehouses(x: OutputData, y: OutputData): number {
    if (x.totalAmount > y.totalAmount) {
      return -1;
    }
    if (x.totalAmount < y.totalAmount) {
      return 1;
    }
    if (x.warehouseName.toLowerCase() > y.warehouseName.toLowerCase()) {
      return -1;
    }
    if (x.warehouseName.toLowerCase() < y.warehouseName.toLowerCase()) {
      return 1;
    }
    return 0;
  }

  private sortMaterials(x: MaterialData, y: MaterialData): number {
    {
      if (x.materialId.toLowerCase() > y.materialId.toLowerCase()) {
        return 1;
      }
      if (x.materialId.toLowerCase() < y.materialId.toLowerCase()) {
        return -1;
      }
      return 0;
    }
  }

  private mapToOutput(): void {
    // Get unique warehouses
    const warehouses = new Set<string>();
    this.inputData.forEach((data) => {
      [...data.warehouseData.map((wh) => wh.warehouseName)].forEach((name) =>
        warehouses.add(name)
      );
    });

    // Create Out data objects
    warehouses.forEach((name) => {
      const outData = new OutputData();
      outData.warehouseName = name;
      this.outputData.push(outData);
    });

    //
    this.inputData.forEach((data) => {
      data.warehouseData.forEach((whData) => {
        const warehouse = this.outputData.find(
          (od) => od.warehouseName === whData.warehouseName
        );
        if (warehouse) {
          const material = new MaterialData();
          material.materialId = data.materialId;
          material.materialAmount = whData.amount;
          warehouse.materials.push(material);
        }
      });
    });
  }

  private onFileLoad(data: string): void {
    this.isDataFinished$.next(false);
    const lines: string[] = data
      .split('\n')
      .filter((line) => !line.startsWith('#'));
    this.mapToInputData(lines);
    this.isDataFinished$.next(true);
  }

  private mapToInputData(lines: string[]) {
    const linesWithProbel: string[] = [];
    lines.forEach((line, index) => {
      try {
        this.inputData.push(this.validateLine(line));
      } catch (error) {
        console.log(`line ${index}: ${error}`);
        linesWithProbel.push(line);
      }
    });
    if (linesWithProbel.length > 2) {
      this.fixData(linesWithProbel);
    }
  }

  private fixData(linesWithProbel: string[]) {
    const mergedLines = [];
    for (let i = 0; i < linesWithProbel.length - 1; i += 2) {
      mergedLines.push(linesWithProbel[i].concat(linesWithProbel[i + 1]));
    }
    this.mapToInputData(mergedLines);
  }

  private validateLine(line: string): InputData {
    const inputData = new InputData();
    const lineElements = line.split(';');
    if (lineElements.length < 3) {
      throw new Error(
        `Validation failed, number of elements in line inncorect ${lineElements.length}`
      );
    }
    inputData.materialName = lineElements.shift();
    const materialId = lineElements.shift();

    if (
      typeof materialId !== 'string' ||
      !materialId.includes('-') ||
      materialId.split('-').length !== 2
    ) {
      throw new Error('Validation Failed, material id is inncorect');
    }
    inputData.materialId = materialId;

    inputData.warehouseData = this.validateWarehouseElements(
      lineElements.shift()
    );

    return inputData;
  }

  private validateWarehouseElements(
    line: string
  ): { warehouseName: string; amount: number }[] {
    const result: { warehouseName: string; amount: number }[] = [];
    const elements = line.split('|');
    elements.forEach((el) => {
      let item = el.split(',');
      // Check number of elements in data
      if (item.length < 2) {
        throw new Error('Warehouse item broken, missing data');
      }
      // Check empty elements
      item = item.map((i) => i.trim());
      item.forEach((i) => {
        if (i === '') {
          throw new Error('Warehouse item element missing');
        }
      });
      // Fix missing '-' in warehouse name
      if (!item[0].includes('-') && item[0].length === 3) {
        item[0] = item[0].substr(0, 2) + '-' + item[0].substr(2, 1);
      }
      result.push({ warehouseName: item[0], amount: +item[1] });
    });

    return result;
  }
}
