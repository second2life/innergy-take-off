import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { DragAndDropDirective } from './shared/dragAndDrop.directive';

@NgModule({
  declarations: [AppComponent, DragAndDropDirective],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
