export class InputData {
  materialName: string;
  materialId: string;
  warehouseData: WareHouseData[] = [];
}
export class WareHouseData {
  warehouseName: string;
  amount: number;
}
