export class OutputData {
  warehouseName: string;
  materials: MaterialData[] = [];

  public get totalAmount(): number {
    let result = 0;
    this.materials.forEach((material) => {
      result += material.materialAmount;
    });

    return result;
  }
}

export class MaterialData {
  materialId: string;
  materialAmount: number;
}
