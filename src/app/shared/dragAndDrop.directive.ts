import {
  Directive,
  EventEmitter,
  HostListener,
  Output,
  HostBinding,
} from '@angular/core';

@Directive({
  selector: '[appDragAndDrop]',
})
export class DragAndDropDirective {
  @Output() public fileLoaded = new EventEmitter();
  constructor() {}

  @HostListener('dragover', ['$event']) public onDragOver(evt) {
    evt.preventDefault();
    evt.stopPropagation();
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(evt) {
    evt.preventDefault();
    evt.stopPropagation();
  }

  @HostListener('drop', ['$event']) public onDrop(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    const file = evt.dataTransfer.files;
    if (file.length) {
      this.fileLoaded.emit(file);
    }
  }
}
